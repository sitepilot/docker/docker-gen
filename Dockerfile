FROM jwilder/docker-gen:latest
LABEL org.label-schema.schema-version="1.0.0"
LABEL org.label-schema.vendor="Sitepilot"
LABEL org.label-schema.name="docker-gen"

RUN apk --update add bash curl jq && rm -rf /var/cache/apk/*
COPY docker-label-sighup /usr/bin/docker-label-sighup

RUN chmod 777 /usr/bin/docker-label-sighup